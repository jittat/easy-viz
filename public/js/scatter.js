ScatterChart = function(config) {
  this.config = config;
  this.data = undefined;
  this.x_axis = "";
  this.y_axis = "";
};

ScatterChart.prototype.chartConfig = {
  width: 600,
  height: 400
};

ScatterChart.prototype.onData = function(cb) {
  if(this.data) { cb(); return; }
  var self = this;
  d3.csv(self.config.data, function(data) {
    self.data = data;
    cb();
  });
};

ScatterChart.prototype.getXValue = function(d) {
  return parseFloat(d[this.x_axis]);
};

ScatterChart.prototype.getYValue = function(d) {
  return parseFloat(d[this.y_axis]);
};

ScatterChart.prototype.getRValue = function(d) {
  return 5;
};

ScatterChart.prototype.getMaxValue = function(valFunc) {
  var mx = 0;
  this.data.forEach(function(d) {
    var x = valFunc(d);
    if(x > mx) {
      mx = x;
    }
  });
  return mx;
};

ScatterChart.prototype.updateScale = function() {
  var self = this;
  self.maxX = self.getMaxValue(function(d) { return self.getXValue(d) });
  self.maxY = self.getMaxValue(function(d) { return self.getYValue(d) });
  self.maxScaleX = Math.ceil(self.maxX * 1.1);
  self.maxScaleY = Math.ceil(self.maxY * 1.1);
};

ScatterChart.prototype.setUpAxisSelect = function(sel) {
  d3.select(sel)
    .selectAll("option")
    .data(this.config.axis_choices)
    .enter()
    .append("option")
    .attr("value", function(d,i) {
      return d;
    })
    .text(function(d,i) { return d; });
};

ScatterChart.prototype.init = function() {
  var self = this;
  
  this.setUpAxisSelect("#x_axis_select_id");
  this.setUpAxisSelect("#y_axis_select_id");

  this.x_axis = this.config.axis_choices[0];
  this.y_axis = this.config.axis_choices[1];

  d3.select("#y_axis_select_id").property("value", this.y_axis);
  
  var updateAxis = function() {
    self.x_axis = d3.select("#x_axis_select_id").property("value");
    self.y_axis = d3.select("#y_axis_select_id").property("value");
    self.render();
  };
  
  d3.select("#x_axis_select_id").on("change", updateAxis);
  d3.select("#y_axis_select_id").on("change", updateAxis);
};

ScatterChart.prototype.render = function() {
  var self = this;
  var height = self.chartConfig.height;
  var width = self.chartConfig.width;
  d3.select(".chart")
    .attr("width", width)
    .attr("height", height);

  this.onData(function() {
    var data = self.data;

    self.updateScale();

    var setAttr = function(sel) {
      sel.attr("cx", function( d, i ) { return self.getXValue(d) / self.maxScaleX * width; })
        .attr("cy", function( d, i ) { return height - (self.getYValue(d) / self.maxScaleY * height); })
        .attr("r", function( d, i ) { return self.getRValue(d); })
        .attr("stroke", "#f07000")
        .attr("stroke-opacity", 0.3)
        .attr("fill", "#ff7700")
        .attr("fill-opacity", 0.3);
    };

    setAttr(d3.select(".chart")
            .selectAll("circle")
            .data(data)
            .enter()
            .append("circle"));

    setAttr(d3.select(".chart")
            .selectAll("circle")
            .data(data)
            .transition());
  });
};
